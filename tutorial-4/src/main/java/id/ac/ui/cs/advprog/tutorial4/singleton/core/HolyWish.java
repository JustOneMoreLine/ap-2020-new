package id.ac.ui.cs.advprog.tutorial4.singleton.core;

public class HolyWish {

    private String wish;

    // TODO complete me with any Singleton approach
    // Eager Method Singleton

    private static HolyWish IamThou = new HolyWish();

    private HolyWish(){}

    public static HolyWish getInstance(){ return IamThou; }

    public String getWish() {
        return wish;
    }

    public void setWish(String wish) {
        this.wish = wish;
    }

    @Override
    public String toString() {
        return wish;
    }
}
