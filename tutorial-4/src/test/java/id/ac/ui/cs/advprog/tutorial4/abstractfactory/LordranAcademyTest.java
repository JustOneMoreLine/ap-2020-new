package id.ac.ui.cs.advprog.tutorial4.abstractfactory;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.LordranAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.KnightAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.armor.MetalArmor;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.armor.ShiningArmor;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.skill.ShiningForce;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.skill.ThousandYearsOfPain;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.weapon.ShiningBuster;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.weapon.ThousandJacker;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MajesticKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.Knight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MetalClusterKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.SyntheticKnight;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


public class LordranAcademyTest {
    KnightAcademy lordranAcademy;
    Knight majesticKnight;
    Knight metalClusterKnight;
    Knight syntheticKnight;

    @BeforeEach
    public void setUp() {
        // TODO setup me
        lordranAcademy = new LordranAcademy();
        majesticKnight = lordranAcademy.getKnight("majestic");
        metalClusterKnight = lordranAcademy.getKnight("metal cluster");
        syntheticKnight = lordranAcademy.getKnight("synthetic");
    }

    @Test
    public void checkKnightInstances() {
        // TODO create test
        assertTrue(majesticKnight instanceof  MajesticKnight);
        assertTrue(metalClusterKnight instanceof MetalClusterKnight);
        assertTrue(syntheticKnight instanceof SyntheticKnight);
    }

    @Test
    public void checkKnightNames() {
        // TODO create test
        assertEquals(majesticKnight.getName(), "Holy Majestic");
        assertEquals(metalClusterKnight.getName(), "Holy Metalhead");
        assertEquals(syntheticKnight.getName(), "Holy Synth");
    }

    @Test
    public void checkKnightDescriptions() {
        // TODO create test
        majesticKnight.setName("Test1");
        metalClusterKnight.setName("Test2");
        syntheticKnight.setName("Test3");

        assertEquals(majesticKnight.getName(), "Test1");
        assertTrue(majesticKnight instanceof Knight);
        assertTrue(majesticKnight instanceof MajesticKnight);
        assertTrue(majesticKnight.getArmor() instanceof ShiningArmor);
        assertFalse(majesticKnight.getSkill() instanceof ShiningForce);
        assertTrue(majesticKnight.getWeapon() instanceof ShiningBuster);

        assertEquals(metalClusterKnight.getName(), "Test2");
        assertTrue(metalClusterKnight instanceof Knight);
        assertTrue(metalClusterKnight instanceof MetalClusterKnight);
        assertTrue(metalClusterKnight.getArmor() instanceof ShiningArmor);
        assertTrue(metalClusterKnight.getSkill() instanceof ShiningForce);
        assertFalse(metalClusterKnight.getWeapon() instanceof ShiningBuster);

        assertEquals(syntheticKnight.getName(), "Test3");
        assertTrue(syntheticKnight instanceof Knight);
        assertTrue(syntheticKnight instanceof SyntheticKnight);
        assertFalse(syntheticKnight.getArmor() instanceof ShiningArmor);
        assertTrue(syntheticKnight.getSkill() instanceof ShiningForce);
        assertTrue(syntheticKnight.getWeapon() instanceof ShiningBuster);
    }
    // Added test
    @Test
    public void TestGetName(){
        assertEquals(lordranAcademy.getName(), "Lordran");
    }
}
