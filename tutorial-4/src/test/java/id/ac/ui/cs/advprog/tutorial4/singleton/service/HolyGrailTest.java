package id.ac.ui.cs.advprog.tutorial4.singleton.service;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.service.AcademyService;
import id.ac.ui.cs.advprog.tutorial4.singleton.core.HolyWish;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.junit.jupiter.api.Assertions.*;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class HolyGrailTest {

    // TODO create tests

    @InjectMocks
    private HolyGrail holyGrail;

    @Test
    public void testMakeAWish(){
        holyGrail.makeAWish("Island full of money");
        assertEquals(holyGrail.getHolyWish().toString(), "Island full of money");
    }

    @Test
    public void testGetHolyWish(){
        assertTrue(holyGrail.getHolyWish() instanceof HolyWish);
        HolyWish a = holyGrail.getHolyWish();
        assertEquals(a, holyGrail.getHolyWish());
    }
}
