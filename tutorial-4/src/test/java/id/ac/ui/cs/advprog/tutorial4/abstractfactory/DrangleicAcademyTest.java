package id.ac.ui.cs.advprog.tutorial4.abstractfactory;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.DrangleicAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.KnightAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.armor.MetalArmor;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.skill.ThousandYearsOfPain;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.weapon.ThousandJacker;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MajesticKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.Knight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MetalClusterKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.SyntheticKnight;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


public class DrangleicAcademyTest {
    KnightAcademy drangleicAcademy;
    Knight majesticKnight;
    Knight metalClusterKnight;
    Knight syntheticKnight;

    @BeforeEach
    public void setUp() {
        // TODO setup me
        drangleicAcademy = new DrangleicAcademy();
        majesticKnight = drangleicAcademy.getKnight("majestic");
        metalClusterKnight = drangleicAcademy.getKnight("metal cluster");
        syntheticKnight = drangleicAcademy.getKnight("synthetic");
    }

    @Test
    public void checkKnightInstances() {
        // TODO create test
        assertTrue(majesticKnight instanceof  MajesticKnight);
        assertTrue(metalClusterKnight instanceof MetalClusterKnight);
        assertTrue(syntheticKnight instanceof SyntheticKnight);
    }

    @Test
    public void checkKnightNames() {
        // TODO create test
        assertEquals(majesticKnight.getName(), "Sir Majestic");
        assertEquals(metalClusterKnight.getName(), "Sir Metalhead");
        assertEquals(syntheticKnight.getName(), "Sir Synth");
    }

    @Test
    public void checkKnightDescriptions() {
        // TODO create test
        majesticKnight.setName("Test1");
        metalClusterKnight.setName("Test2");
        syntheticKnight.setName("Test3");

        assertEquals(majesticKnight.getName(), "Test1");
        assertTrue(majesticKnight instanceof Knight);
        assertTrue(majesticKnight instanceof MajesticKnight);
        assertTrue(majesticKnight.getArmor() instanceof MetalArmor);
        assertFalse(majesticKnight.getSkill() instanceof ThousandYearsOfPain);
        assertTrue(majesticKnight.getWeapon() instanceof ThousandJacker);

        assertEquals(metalClusterKnight.getName(), "Test2");
        assertTrue(metalClusterKnight instanceof Knight);
        assertTrue(metalClusterKnight instanceof MetalClusterKnight);
        assertTrue(metalClusterKnight.getArmor() instanceof MetalArmor);
        assertTrue(metalClusterKnight.getSkill() instanceof ThousandYearsOfPain);
        assertFalse(metalClusterKnight.getWeapon() instanceof ThousandJacker);

        assertEquals(syntheticKnight.getName(), "Test3");
        assertTrue(syntheticKnight instanceof Knight);
        assertTrue(syntheticKnight instanceof SyntheticKnight);
        assertFalse(syntheticKnight.getArmor() instanceof MetalArmor);
        assertTrue(syntheticKnight.getSkill() instanceof ThousandYearsOfPain);
        assertTrue(syntheticKnight.getWeapon() instanceof ThousandJacker);
    }

    // Added test
    @Test
    public void TestGetName(){
        assertEquals(drangleicAcademy.getName(), "Drangleic");
    }
}
