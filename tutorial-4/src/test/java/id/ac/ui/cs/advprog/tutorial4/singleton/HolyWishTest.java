package id.ac.ui.cs.advprog.tutorial4.singleton;

import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.List;

import id.ac.ui.cs.advprog.tutorial4.singleton.core.HolyWish;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class HolyWishTest {

    private Class<?> holyWishClass;
    private HolyWish holyWish;

    @BeforeEach
    public void setUp() throws Exception {
        holyWishClass = Class.forName(HolyWish.class.getName());
        holyWish = HolyWish.getInstance();
    }

    @Test
    public void testNoPublicConstructors() {
        List<Constructor> constructors = Arrays.asList(holyWishClass.getDeclaredConstructors());

        boolean check = constructors.stream()
                .anyMatch(c -> !Modifier.isPrivate(c.getModifiers()));

        assertFalse(check);
    }

    @Test
    public void testGetInstanceShouldReturnSingletonInstance() {
        // TODO create test
        HolyWish a = holyWish.getInstance();
        assertEquals(holyWish, a);
    }

    @Test
    public void testWish(){
        // TODO create test
        HolyWish a = holyWish.getInstance();
        holyWish.setWish("A wish upon the stars");
        assertEquals(holyWish.getWish(), a.getWish());
        a.setWish("Lots of money");
        assertEquals(holyWish.getWish(), a.getWish());
    }

    @Test
    public void testWishExist(){
        // TODO create test
        holyWish.setWish("Wish exists");
        assertTrue(holyWish.getWish() instanceof String);
        assertEquals(holyWish.getWish(), "Wish exists");
    }

}
