package id.ac.ui.cs.advprog.tutorial4.abstractfactory.service;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.DrangleicAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.KnightAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.LordranAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.Knight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MajesticKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MetalClusterKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.SyntheticKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.repository.AcademyRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class AcademyServiceImplTest {

    @Mock
    private AcademyRepository academyRepository;

    @InjectMocks
    private AcademyServiceImpl academyService;

    // TODO create tests

    @BeforeEach
    public void setUp(){
        academyRepository = new AcademyRepository();
        academyService = new AcademyServiceImpl(academyRepository);
    }
    @Test
    public void TestProduceKnights(){
        KnightAcademy b = new DrangleicAcademy();
        academyRepository.addKnightAcademy("a", b);
        academyService.produceKnight("a","majestic");
        assertTrue(academyService.getKnight() instanceof MajesticKnight);
        academyService.produceKnight("a","metal cluster");
        assertTrue(academyService.getKnight() instanceof MetalClusterKnight);
        academyService.produceKnight("a","synthetic");
        assertTrue(academyService.getKnight() instanceof SyntheticKnight);
    }

    @Test
    public void TestGetKnightAcademies(){
        KnightAcademy a = new DrangleicAcademy();
        KnightAcademy b = new LordranAcademy();
        academyRepository.addKnightAcademy("a", a);
        academyRepository.addKnightAcademy("b", b);
        assertTrue(academyService.getKnightAcademies() instanceof List);
        assertEquals(academyService.getKnightAcademies().size(), 4);
    }

    @Test
    public void TestGetKnight() {
        KnightAcademy b = new DrangleicAcademy();
        academyRepository.addKnightAcademy("a", b);
        academyService.produceKnight("a","majestic");
        assertTrue(academyService.getKnight() instanceof MajesticKnight);
    }
}
