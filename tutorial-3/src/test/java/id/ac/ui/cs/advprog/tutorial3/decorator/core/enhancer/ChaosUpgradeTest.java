package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;


import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Gun;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ChaosUpgradeTest {

    private ChaosUpgrade chaosUpgrade;
    @BeforeEach
    public void setUp(){
        chaosUpgrade = new ChaosUpgrade(new Gun());
    }

    @Test
    public void testMethodGetWeaponName(){
        //TODO: Complete me
        assertEquals(chaosUpgrade.getName(), "Gun");
    }

    @Test
    public void testGetMethodWeaponDescription(){
        //TODO: Complete me
        assertEquals(chaosUpgrade.getDescription(), "A tool that shoots projectiles called bullets, a byproduct of science and war that comes with a BANG. Ok I'll see my way out.....");
    }

    @Test
    public void testMethodGetWeaponValue(){
        //TODO: Complete me
        assertTrue(chaosUpgrade.getWeaponValue() >= 70 && chaosUpgrade.getWeaponValue() <= 75);
    }

}
