package id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
public class GunTest {

    private Weapon weapon;

    @BeforeEach
    public void setUp(){
        weapon = new Gun();
    }

    @Test
    public void testMethodGetWeaponName(){
        //TODO: Complete me
        assertEquals(weapon.getName(), "Gun");
    }

    @Test
    public void testMethodGetWeaponDescription(){
        //TODO: Complete me
        assertEquals(weapon.getDescription(), "A tool that shoots projectiles called bullets, a byproduct of science and war that comes with a BANG. Ok I'll see my way out.....");
    }

    @Test
    public void testMethodGetWeaponValue(){
        //TODO: Complete me
        assertEquals(weapon.getWeaponValue(), 20);
    }
}
