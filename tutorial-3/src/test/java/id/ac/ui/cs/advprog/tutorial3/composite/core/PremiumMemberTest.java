package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class PremiumMemberTest {
    private Member member;
    private Guild guild;
    private Member guildMaster;

    @BeforeEach
    public void setUp() {
        member = new PremiumMember("Aqua", "Goddess");
        guildMaster = new PremiumMember("Chinchin", "Being a chinchin");
        guild = new Guild(guildMaster);
    }

    @Test
    public void testMethodGetName()
    {
        //TODO: Complete me
        assertEquals(member.getName(), "Aqua");
    }

    @Test
    public void testMethodGetRole() {
        //TODO: Complete me
        assertEquals(member.getRole(), "Goddess");
    }

    @Test
    public void testMethodAddChildMember() {
        //TODO: Complete me
        Member addMember = new OrdinaryMember("Kazuma", "True Gender Equalist");
        member.addChildMember(addMember);
        assertTrue(member.getChildMembers().contains(addMember));
    }

    @Test
    public void testMethodRemoveChildMember() {
        //TODO: Complete me
        Member addMember = new OrdinaryMember("Kazuma", "True Gender Equalist");
        member.addChildMember(addMember);
        assertTrue(member.getChildMembers().contains(addMember));
        member.removeChildMember(addMember);
        assertTrue(member.getChildMembers().contains(addMember) == false);
    }

    @Test
    public void testNonGuildMasterCanNotAddChildMembersMoreThanThree() {
        //TODO: Complete me
        Member addMember1 = new OrdinaryMember("Kazuma", "True Gender Equalist");
        guild.addMember(member, addMember1);
        Member addMember2 = new OrdinaryMember("Megumin", "Underaged");
        guild.addMember(member, addMember2);
        Member addMember3 = new OrdinaryMember("Darkness", "Masochist");
        guild.addMember(member, addMember3);
        assertEquals(member.getChildMembers().size(), 3);
        Member addMemberFail = new OrdinaryMember("Subaru", "Bye Rem");
        guild.addMember(member, addMemberFail);
        assertEquals(member.getChildMembers().size(), 3);
        assertTrue(member.getChildMembers().contains(addMemberFail) == false);
    }

    @Test
    public void testGuildMasterCanAddChildMembersMoreThanThree() {
        //TODO: Complete me
        Member addMember1 = new OrdinaryMember("Kazuma", "True Gender Equalist");
        Member addMember2 = new OrdinaryMember("Megumin", "Underaged");
        Member addMember3 = new OrdinaryMember("Darkness", "Masochist");
        Member addMemberSuccess = new OrdinaryMember("Subaru", "Bye Rem");
        assertEquals(guildMaster.getChildMembers().size(), 0);
        guild.addMember(guildMaster, addMember1);
        guild.addMember(guildMaster, addMember2);
        guild.addMember(guildMaster, addMember3);
        guild.addMember(guildMaster, addMemberSuccess);
        assertEquals(guildMaster.getChildMembers().size(), 4);
    }
}
