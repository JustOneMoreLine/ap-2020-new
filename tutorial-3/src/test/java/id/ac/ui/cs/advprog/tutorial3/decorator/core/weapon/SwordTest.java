package id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SwordTest {

    private Weapon weapon;

    @BeforeEach
    public void setUp(){
        weapon = new Sword();
    }

    @Test
    public void testMethodGetWeaponName(){
        //TODO: Complete me
        assertEquals(weapon.getName(), "Sword");
    }

    @Test
    public void testMethodGetWeaponDescription(){
        //TODO: Complete me
        assertEquals(weapon.getDescription(), "The next iteration of the wooden club, but with metal and a sharp edge, makes a \"Whoosh whoosh\" sound when swinged.");
    }

    @Test
    public void testMethodGetWeaponValue(){
        //TODO: Complete me
        assertEquals(weapon.getWeaponValue(), 25);
    }
}
