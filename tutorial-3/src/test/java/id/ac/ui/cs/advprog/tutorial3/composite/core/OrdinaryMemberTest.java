package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class OrdinaryMemberTest {
    private Member member;

    @BeforeEach
    public void setUp() {
        member = new OrdinaryMember("Asuna", "Waifu");
    }

    @Test
    public void testMethodGetName() {
        //TODO: Complete me
        assertEquals(member.getName(),"Asuna");
    }

    @Test
    public void testMethodGetRole() {
        //TODO: Complete me
        assertEquals(member.getRole(), "Waifu");
    }

    @Test
    public void testMethodAddRemoveChildMemberDoNothing() {
        //TODO: Complete me
        Member addMember = new OrdinaryMember("Yui","commitment");
        member.addChildMember(addMember);
        assertTrue(member.getChildMembers().isEmpty());
        member.removeChildMember(addMember);
        assertTrue(member.getChildMembers().isEmpty());

    }
}
