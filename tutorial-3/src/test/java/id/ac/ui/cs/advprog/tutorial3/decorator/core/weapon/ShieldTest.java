package id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ShieldTest {

    private Weapon weapon;

    @BeforeEach
    public void setUp(){
        weapon = new Shield();
    }

    @Test
    public void testMethodGetWeaponName(){
        //TODO: Complete me
        assertEquals(weapon.getName(), "Shield");
    }

    @Test
    public void testMethodGetWeaponDescription(){
        //TODO: Complete me
        assertEquals(weapon.getDescription(), "A big plate of wood and metal, used to protect the one the wearers care about most, usually himself. Selfish pricks.");
    }

    @Test
    public void testMethodGetWeaponValue(){
        //TODO: Complete me
        assertEquals(weapon.getWeaponValue(), 10);
    }
}
