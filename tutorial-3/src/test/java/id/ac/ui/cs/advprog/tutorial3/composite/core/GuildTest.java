package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class GuildTest {
    private Guild guild;
    private Member guildMaster;

    @BeforeEach
    public void setUp() {
        guildMaster = new PremiumMember("Naruto", "Master");
        guild = new Guild(guildMaster);
    }

    @Test
    public void testMethodAddMember() {
        //TODO: Complete me
        Member useless = new OrdinaryMember("Useless", "useless");
        guild.addMember(guildMaster, useless);
        List<Member> test = guild.getMemberList();
        assertTrue(test.contains(useless));
    }

    @Test
    public void testMethodRemoveMember() {
        //TODO: Complete me
        Member useless = new OrdinaryMember("Useless", "useless");
        guild.addMember(guildMaster, useless);
        List<Member> test = guild.getMemberList();
        assertTrue(test.contains(useless));
        guild.removeMember(guildMaster, useless);
        assertTrue(!guild.getMemberList().contains(useless));
    }

    @Test
    public void testMethodMemberHierarchy() {
        Member sasuke = new OrdinaryMember("Sasuke", "Uchiha");
        guild.addMember(guildMaster, sasuke);
        assertEquals(guildMaster, guild.getMemberHierarchy().get(0));
        assertEquals(sasuke, guild.getMemberHierarchy().get(0).getChildMembers().get(0));
    }

    @Test
    public void testMethodGetMember() {
        //TODO: Complete me
        Member useless = new OrdinaryMember("Useless", "useless");
        guild.addMember(guildMaster, useless);
        List<Member> test = guild.getMemberList();
        assertTrue(test.contains(useless));
        Member testMember = guild.getMember("Useless", "useless");
        assertEquals(testMember, useless);
    }
}
