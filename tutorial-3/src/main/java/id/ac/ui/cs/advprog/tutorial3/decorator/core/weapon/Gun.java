package id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon;

public class Gun extends Weapon {
        //TODO: Complete me
    protected final String GUN_NAME = "Gun";
    protected final String GUN_DESC = "A tool that shoots projectiles called bullets, a byproduct of science and war that comes with a BANG. Ok I'll see my way out.....";
    protected final int GUN_VAL = 20;
    public Gun(){
        this.weaponName = GUN_NAME;
        this.weaponDescription = GUN_DESC;
        this.weaponValue = GUN_VAL;
    }
}

