package id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon;

public class Longbow extends Weapon {
        //TODO: Complete me
    protected final String LB_NAME = "Longbow";
    protected final String LB_DESC = "A type of bow, pack more punch than regular bows.";
    protected final int LB_VAL = 15;
    public Longbow(){
        this.weaponName = LB_NAME;
        this.weaponDescription = LB_DESC;
        this.weaponValue = LB_VAL;
    }
}
