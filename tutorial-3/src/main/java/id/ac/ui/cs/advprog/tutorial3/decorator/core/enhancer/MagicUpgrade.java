package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;

public class MagicUpgrade extends Weapon {

    Weapon weapon;
    int enchance;
    public MagicUpgrade(Weapon weapon) {
        Integer add = (int)(5*Math.random());
        add += 15;
        this.weapon= weapon;
        this.enchance = add;
    }

    @Override
    public String getName() {
        return weapon.getName();
    }

    // Senjata bisa dienhance hingga 15-20 ++
    @Override
    public int getWeaponValue() {
        //TODO: Complete me
        return this.weapon.getWeaponValue() + enchance;
    }

    @Override
    public String getDescription() {
        //TODO: Complete me
        return this.weapon.getDescription();
    }
}
