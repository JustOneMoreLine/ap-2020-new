package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

public class ChaosUpgrade extends Weapon {

    Weapon weapon;
    int enhance;

    public ChaosUpgrade(Weapon weapon) {
        Integer add = (int)(5*Math.random());
        add += 50;
        this.enhance = add;
        this.weapon= weapon;

    }

    @Override
    public String getName() {
        return weapon.getName();
    }

    // Senjata bisa dienhance hingga 50-55 ++
    @Override
    public int getWeaponValue() {
        return this.weapon.getWeaponValue() + this.enhance;
    }

    @Override
    public String getDescription() {
        //TODO: Complete me
        return this.weapon.getDescription();
    }
}
