package id.ac.ui.cs.advprog.tutorial3.composite.core;

import java.util.ArrayList;
import java.util.List;

public class PremiumMember implements Member {
        //TODO: Complete me
    private String name;
    private String role;
    private List<Member> members;
    public PremiumMember(String name, String role){
        this.name = name;
        this.role = role;
        this.members = new ArrayList<>();
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String getRole() {
        return this.role;
    }

    @Override
    public void addChildMember(Member member) {
        this.members.add(member);
    }

    @Override
    public void removeChildMember(Member member) {
        if(this.members.contains(member)) this.members.remove(member);
    }

    @Override
    public List<Member> getChildMembers() {
        return this.members;
    }
}
