package id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon;

public class Sword extends Weapon {
        //TODO: Complete me
    protected final String SW_NAME = "Sword";
    protected final String SW_DESC = "The next iteration of the wooden club, but with metal and a sharp edge, makes a \"Whoosh whoosh\" sound when swinged.";
    protected final int SW_VAL = 25;
    public Sword(){
        this.weaponName = SW_NAME;
        this.weaponDescription = SW_DESC;
        this.weaponValue = SW_VAL;
    }
}
