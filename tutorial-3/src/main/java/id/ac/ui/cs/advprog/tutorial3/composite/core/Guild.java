package id.ac.ui.cs.advprog.tutorial3.composite.core;

import java.util.*;

public class Guild {
    private Member guildMaster;
    private List<Member> memberList;

    public Guild(Member master) {
        this.guildMaster = master;
        this.memberList = new ArrayList<>();
        this.memberList.add(guildMaster);
    }

    public void addMember(Member parent, Member child) {
        //TODO: Complete me
        if(guildMaster.equals(parent)){
            guildMaster.addChildMember(child);
            getMemberListHelper(child);
        }else if(parent instanceof PremiumMember){
            if(parent.getChildMembers().size() < 3){
                parent.addChildMember(child);
                getMemberListHelper(child);
            }
        }
    }

    public void removeMember(Member parent, Member child) {
        //TODO: Complete me
        if (parent instanceof PremiumMember) {
            if(parent.getChildMembers().contains(child)) parent.removeChildMember(child);
            if(this.memberList.contains(child)) this.memberList.remove(child);
        }
    }

    public List<Member> getMemberHierarchy() {
        return new ArrayList<>(Arrays.asList(guildMaster));
    }

    public List<Member> getMemberList() {
        //TODO: Complete me
        return this.memberList;
    }

    public Member getMember(String name, String role) {
        return getMemberHelper(this.guildMaster, name, role);
    }

    private void getMemberListHelper(Member member) {
        memberList.add(member);
        for (Member m: member.getChildMembers()) {
            this.getMemberListHelper(m);
        }
    }

    private Member getMemberHelper(Member member, String name, String role) {
        if (member.getName().equals(name) && member.getRole().equals(role)) {
            return member;
        }

        if (member instanceof OrdinaryMember) {
            return null;
        }

        for (Member m: member.getChildMembers()) {
            Member find = this.getMemberHelper(m, name, role);
            if (find != null) {
                return find;
            }
        }

        return null;
    }
}
