package id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon;

public class Shield extends Weapon {
        //TODO: Complete me
    protected final String SH_NAME = "Shield";
    protected final String SH_DESC = "A big plate of wood and metal, used to protect the one the wearers care about most, usually himself. Selfish pricks.";
    protected final int SH_VAL = 10;
    public Shield(){
        this.weaponName = SH_NAME;
        this.weaponDescription = SH_DESC;
        this.weaponValue = SH_VAL;
    }
}
