package id.ac.ui.cs.advprog.tutorial1.strategy.core;

import org.springframework.beans.factory.annotation.Autowired;

public class MysticAdventurer extends Adventurer {
        //ToDo: Complete me
        /*@Autowired
        private AttackBehavior attackBehavior;
    @Autowired
    private DefenseBehavior defenseBehavior;*/
    public MysticAdventurer(){
        this.setAttackBehavior(new AttackWithMagic());
        this.setDefenseBehavior(new DefendWithShield());
    }

    @Override
    public String getAlias() {
        return "HARRY POTTER";
    }
}
