package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithShield implements DefenseBehavior {
        //ToDo: Complete me

    @Override
    public String defend() {
        return "PRISON SHIELD";
    }

    @Override
    public String getType() {
        return "Shield";
    }
}
