package id.ac.ui.cs.advprog.tutorial1.observer.core;

import java.util.ArrayList;

public class AgileAdventurer extends Adventurer {

        public AgileAdventurer(Guild guild) {
                this.name = "Agile";
                //ToDo: Complete Me
                this.guild = guild;
                this.quests = new ArrayList<>();
                guild.add(this);
        }

        //ToDo: Complete Me

        @Override
        public void update() {
                this.quests.add(guild.getQuest());
        }
}
