package id.ac.ui.cs.advprog.tutorial1.strategy.core;

import org.springframework.beans.factory.annotation.Autowired;

public class AgileAdventurer extends Adventurer {
        //ToDo: Complete
    /*@Autowired
    private AttackBehavior attackBehavior;
    @Autowired
    private DefenseBehavior defenseBehavior;*/
    public AgileAdventurer(){
        this.setAttackBehavior(new AttackWithGun());
        this.setDefenseBehavior(new DefendWithBarrier());
    }
    @Override
    public String getAlias() {
        return "RUN GUN";
    }
}
