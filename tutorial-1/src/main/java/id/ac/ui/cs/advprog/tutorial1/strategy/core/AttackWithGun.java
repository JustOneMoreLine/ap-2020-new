package id.ac.ui.cs.advprog.tutorial1.strategy.core;
/*import org.springframework.stereotype.Component;

@Component*/
public class AttackWithGun implements AttackBehavior {
        //ToDo: Complete me

    @Override
    public String attack() {
        return "I use gun! BANG BANG!";
    }

    @Override
    public String getType() {
        return "Gun";
    }
}
