package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithSword implements AttackBehavior {
        //ToDo: Complete me

    @Override
    public String attack() {
        return "I use ma SWORD! SWORD! DO YOU LIKE MY SWORD! SWORD!";
    }

    @Override
    public String getType() {
        return "Sword";
    }
}
