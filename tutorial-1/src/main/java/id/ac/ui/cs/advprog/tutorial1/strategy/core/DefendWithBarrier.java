package id.ac.ui.cs.advprog.tutorial1.strategy.core;
/*import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
@Component*/
public class DefendWithBarrier implements DefenseBehavior {
        //ToDo: Complete me

    @Override
    public String defend() {
        return "Ability card activate! Barrier!";
    }

    @Override
    public String getType() {
        return "Barrier";
    }
}
