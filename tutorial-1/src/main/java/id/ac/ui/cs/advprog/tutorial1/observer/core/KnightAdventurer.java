package id.ac.ui.cs.advprog.tutorial1.observer.core;

import java.util.ArrayList;

public class KnightAdventurer extends Adventurer {

        public KnightAdventurer(Guild guild) {
                this.name = "Knight";
                //ToDo: Complete Me
                this.guild = guild;
                this.quests = new ArrayList<>();
                guild.add(this);
        }

        //ToDo: Complete Me

        @Override
        public void update() {
                this.quests.add(guild.getQuest());
        }
}
