package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithArmor implements DefenseBehavior {
        //ToDo: Complete me

    @Override
    public String defend() {
        return "BAKARO! Germany armor is the best in the world!";
    }

    @Override
    public String getType() {
        return "Armor";
    }
}
