package id.ac.ui.cs.advprog.tutorial1.strategy.core;

import org.springframework.beans.factory.annotation.Autowired;

public class KnightAdventurer extends Adventurer {
        //ToDo: Complete me

        /*@Autowired
        private AttackBehavior attackBehavior;
        @Autowired
        private DefenseBehavior defenseBehavior;*/
    public KnightAdventurer(){
        this.setAttackBehavior(new AttackWithSword());
        this.setDefenseBehavior(new DefendWithArmor());
    }

    @Override
    public String getAlias() {
        return "STONEWALL";
    }
}
