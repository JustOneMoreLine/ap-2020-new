package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithMagic implements AttackBehavior {
        //ToDo: Complete me

    @Override
    public String attack() {
        return "I use magick! BOOM BOOM!";
    }

    @Override
    public String getType() {
        return "Magic";
    }
}
