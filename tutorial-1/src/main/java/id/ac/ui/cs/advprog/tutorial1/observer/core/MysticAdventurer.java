package id.ac.ui.cs.advprog.tutorial1.observer.core;

import java.util.ArrayList;

public class MysticAdventurer extends Adventurer {

        public MysticAdventurer(Guild guild) {
                this.name = "Mystic";
                //ToDo: Complete Me
                this.guild = guild;
                this.quests = new ArrayList<>();
                guild.add(this);
        }

        //ToDo: Complete Me

        @Override
        public void update() {
                this.quests.add(guild.getQuest());
        }
}
