package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import java.util.ArrayList;

public class ChainSpell implements Spell {
    // TODO: Complete Me
    private ArrayList<Spell> chainSpells;
    public ChainSpell(ArrayList<Spell> spells){
        chainSpells = spells;
    }

    @Override
    public void cast() {
        for(Spell in : chainSpells){
            in.cast();
        }
    }

    @Override
    public void undo() {
        for(int i = chainSpells.size()-1; i >= 0; i--){
            chainSpells.get(i).undo();
        }
    }

    @Override
    public String spellName() {
        return "ChainSpell";
    }
}
