package id.ac.ui.cs.advprog.tutorial2.command.repository;

import id.ac.ui.cs.advprog.tutorial2.command.core.spell.BlankSpell;
import id.ac.ui.cs.advprog.tutorial2.command.core.spell.Spell;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class ContractSeal {

    private Map<String, Spell> spells;
    private Spell latestSpell;
    private boolean justUndo;

    public ContractSeal() {
        spells = new HashMap<>();
        latestSpell = new BlankSpell();
        justUndo = false;
    }

    public void registerSpell(Spell spell) {
        spells.put(spell.spellName(), spell);
    }

    public void castSpell(String spellName) {
        // TODO: Complete me
        Spell spell = spells.get(spellName);
        spell.cast();
        justUndo = false;
        latestSpell = spell;
    }

    public void undoSpell() {
        // TODO: Complete Me
        if (justUndo){
            // do nothing
        }else{
            justUndo = true;
            latestSpell.undo();
        }
    }

    public Collection<Spell> getSpells() { return spells.values(); }
}
